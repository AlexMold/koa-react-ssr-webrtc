import React from 'react'
import {Route, IndexRoute} from 'react-router'
import App from './frontend/containers/App.js'
import Home from './frontend/containers/Home.js'
import About from './frontend/containers/About.js'
import NotFound from './frontend/containers/NotFound.js'

export default (
    <Route path="/" component={App}>
        <IndexRoute component={Home}/>
        <Route path="about" component={About}/>
        <Route path="*" component={NotFound}/>
    </Route>
)