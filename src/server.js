// @flow

import Koa from 'koa'
import serve from 'koa-static-folder'
import createBrowserHistory from 'history'
import { RouterContext, match } from 'react-router'
import { renderToString, renderToStaticMarkup } from 'react-dom/server'
import React from 'react'
import NotFound from './frontend/containers/NotFound'
import routes from './routes'
import Html from './frontend/containers/Html'


const app = new Koa()


app.use(serve('./dist'))

app.use(async(ctx) => {
    // let location = createBrowserHistory(ctx.request.url)

    // console.log('\n\n ctx.request.url \n\n', ctx.request.url);
    // console.log('\n\n location \n\n', location);

    await match({ routes, location: ctx.request.url }, async(error, redirectLocation, renderProps) => {
        // console.log(' renderProps \n\n', renderProps);
        if (redirectLocation) {
            ctx.redirect(redirectLocation.pathname + redirectLocation.search)
        } else if (error || !renderProps) {
            ctx.throw(500, error.message)
        }

        // set proper HTTP code for if matched route wasn't found
        if (renderProps.components.indexOf(NotFound) != -1) {
            ctx.status = 404
        }


        /**
         * must take Store(redux) object
         * 
         */
        const render = () => {
            const content = renderToString(<RouterContext {...renderProps} />)
            const assets = global.assets


            const markup = <Html content={content} />
            const doctype = '<!doctype html>\n'
            const html = renderToStaticMarkup(markup)

            ctx.response.body = doctype + html;
        }

        await render()

    })
});

app.listen(3000, () => console.log('server started 3000'))

export default app