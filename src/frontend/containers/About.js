import React, { Component } from 'react'

export default class About extends Component {
  render () {
    return <div id="about">
      <h1>About Me</h1>
      <p>Hi! My name is Gribcov Alex and I develop software.</p>
      <p>Find more at <a href="https://www.facebook.com/grial.grib">My FB</a></p>
    </div>
  }
}