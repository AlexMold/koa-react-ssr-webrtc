import React, {Component} from 'react'


export default class App extends Component {
  render () {
    return <div id="app">
      <div id="content">
        {this.props.children}
      </div>
    </div>
  }
}