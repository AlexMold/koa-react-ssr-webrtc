import React, {Component} from 'react'

export default class Home extends Component {
  render () {
    return <div id="home">
      <h1>Home</h1>
      <p>This is an example app using Koa and React-Router</p>
      <p>See the source code on <a href="https://github.com/AlexMold/">GitHub</a></p>
    </div>
  }
}