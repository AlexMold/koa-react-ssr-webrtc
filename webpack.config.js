const webpack = require('webpack');
const autoprefixer = require('autoprefixer');

module.exports = {
    entry: "./src/client.js",
    output: {
        path: __dirname + '/dist/',
        filename: "bundle.js"
    },
    module: {
        rules: [{
                test: /\.styl$/,
                use: [
                    'style-loader',
                    'css-loader',
                    'postcss-loader',
                    'stylus-loader'
                ]
            }, //style!css!autoprefixer!stylus
            // { test: /\.jpg$/, loader: "file?name=[path][name].[ext]?[hash]" },
            // { test: /\.png$/, loader: "file?name=[path][name].[ext]?[hash]" },
            {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules|bower_components)/,
                use: ['babel-loader']
            }
        ]
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                IS_BUNDLING_FOR_BROWSER: true
            }
        }),
    ],
    // postcss: [
    //     autoprefixer({ browsers: ['last 2 versions'] })
    // ]
};